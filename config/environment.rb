require 'sinatra/activerecord'
Dir["./models/*.rb"].each {|file| require file }

configure :development do
 set :database, 'sqlite3:dev.db'
 set :show_exceptions, true
end

configure :test do
 set :database, 'sqlite3:test.db'
end
