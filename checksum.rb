require 'open-uri'
require 'json'
require 'digest'
require 'logger'

class Checksum 

  def initialize
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
  end

  def start
    Repo.all.to_a.each do |repo|
      api_url = "https://api.github.com/repos/#{repo.owner.name}/#{repo.name}/tags"
      git_tags = JSON.load(open(api_url))
      actual_tags_names = sanitize_repo_tags(git_tags, repo)
      git_tags.each do |git_tag|
        Thread.new do 
          tag = actual_tags_names.include?(git_tag['name']) ? repo.tags.select {|tag| tag.name == git_tag['name'] }[0] : repo.tags.new
          if tag.commit_sha != git_tag['commit']['sha']
            tag.name = git_tag['name']
            tag.zipball_url = git_tag['zipball_url']
            tag.tarball_url = git_tag['tarball_url']
            tag.commit_sha = git_tag['commit']['sha']
            tag.tarsha = verify(git_tag['tarball_url'], git_tag['name'])
            tag.zipsha = verify(git_tag['zipball_url'], git_tag['name'])
            if tag.save
              @logger.info( "Succefully updated: #{repo.owner.name}/#{repo.name}: tag #{tag.name}" )
            else
              @logger.info( "Some error with: #{repo.owner.name}/#{repo.name}: tag #{tag.name}" )
            end
          end
        end
      end
    end
    Thread.list.each { |t| t.join if t != Thread.current }
  end

  def sanitize_repo_tags(git_tags, repo)
    old_tags = repo.tags.map(&:name) - git_tags.map {|d| d['name'] }
    repo.tags.each { |tag| tag.destroy if old_tags.include?(tag.name) }
    repo.tags.reject { |tag| old_tags.include?(tag.name) }.map { |tag| tag.name }
  end

  def load_file(file_name, link)
    io = open(link).read
    File.write(file_name, io)
  end

  def get_sha(file_name)
    Digest::SHA256.file(file_name).hexdigest
  end

  def delete_files(arr)
    arr.map {|file| File.delete(file) }
  end

  def verify(link, name)
    files = []
    sha_list = Hash.new(0)
    stop = false
    step = 0
    @logger.info("Verify #{name}. Looking for #{link}")
    begin
      step += 1

      raise RuntimeError.new('File loads more than 3 times') if step > 3
      file_name = "#{name}#{step}"

      load_file(file_name, link)
      files.push(file_name)

      sha_list[ get_sha(file_name) ] += 1
      result_sha = sha_list.map{|k,v| k if v > 1}.compact[0]

      if !result_sha.nil? && step > 1
        delete_files(files)
        stop = true
        return result_sha
      end
    rescue RuntimeError => e
      stop = true
      delete_files(files)
      @logger.warn( "#{e.message}: #{link}" )
      raise e
    rescue => e
      @logger.warn(e.message)
      stop = true
      delete_files(files)
    end while !stop
  end
end
