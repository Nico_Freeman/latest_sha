require 'rack/test'
require 'database_cleaner'

ENV['RACK_ENV'] = 'test'

RSpec.configure do |config|
  config.tty = true
  config.formatter = :documentation
  config.include Rack::Test::Methods

  config.before(:suite) { DatabaseCleaner.strategy = :truncation }
  config.before(:each) { DatabaseCleaner.start }
  config.after(:each) { DatabaseCleaner.clean }

  def app
    Sinatra::Application
  end
end

