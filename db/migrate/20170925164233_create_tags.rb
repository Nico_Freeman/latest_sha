class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string :name
      t.string :zipball_url
      t.string :tarball_url
      t.string :zipsha
      t.string :tarsha
      t.string :commit_sha
      t.integer :repo_id
    end
  end
end
