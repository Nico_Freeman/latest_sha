require './spec_helper'
require './app'

RSpec.describe Checksum do
  before(:each) do
    owner = Owner.create(name: 'ruby')
    owner.repos.create(name: 'ruby')
  end

  before { allow(subject).to receive_message_chain(:open, :read).and_return("some data") }

  it "should delete old tags" do
    repo = Repo.first
    repo.tags.create(name: 'old_tag')
    new_tags = [{'name' => 'new_tag' }]

    subject.sanitize_repo_tags(new_tags, repo)
    expect( repo.tags.where(name: 'old_tag') ).to be_empty
  end

  it 'should save and delete files' do
    files_names = %w(1 2 3)

    # [9, 9, 9] because of file lenght
    expect( files_names.map { |n| subject.load_file(n, 'some link') } ).to eq( [9, 9, 9] )
    expect( subject.delete_files( files_names ) ).to eq( [1, 1, 1] )
  end

  it 'should return correct sha' do
    #sha for 'some data'
    correct_sha = '1307990e6ba5ca145eb35e99182a9bec46531bc54ddf656a602c780fa0240dee'

    expect( subject.verify('some url', 'file_name') ).to eq(correct_sha)
  end

  it 'should throw error when file downloaded more than 3 times' do
    hex = Object.new
    def hex.hexdigest ; Random.rand ; end

    allow(Digest::SHA256).to receive_messages( :file => hex )

    expect{ subject.verify('some link', 'file_name') }.to raise_error(RuntimeError)
  end
end
