class Owner < ActiveRecord::Base
  has_many :repos, dependent: :destroy
end
