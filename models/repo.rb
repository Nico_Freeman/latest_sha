class Repo < ActiveRecord::Base
    belongs_to :owner
    has_many :tags, dependent: :destroy
end
